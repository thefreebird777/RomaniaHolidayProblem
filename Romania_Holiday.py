import random, copy

Romania_Map = {'Oradea':[('Zerind',71),('Sibiu',151)],
               'Zerind':[('Arad',75),('Oradea',71)],
               'Arad':[('Zerind',75),('Sibiu',140),('Timisoara',118)],
               'Timisoara':[('Arad',118),('Lugoj',111)],
               'Lugoj':[('Timisoara',111),('Mehadia',75)],
               'Mehadia':[('Lugoj',70),('Drobeta',75)],
               'Drobeta':[('Mehadia',75),('Craiova',120)],
               'Craiova':[('Drobeta',120),('Rimnicu Vilcea',146),('Pitesti',138)],
               'Rimnicu Vilcea':[('Craiova',146),('Sibiu',80),('Pitesti',97)],
               'Sibiu':[('Oradea',151),('Arad',140),('Fagaras',99),('Rimnicu Vilcea',80)],
               'Fagaras':[('Sibiu',99),('Bucharest',211)],
               'Pitesti':[('Rimnicu Vilcea',97),('Craiova',138),('Bucharest',101)],
               'Bucharest':[('Fagaras',211),('Pitesti',101),('Giurgiu',90),('Urziceni',85)],
               'Giurgiu':[('Bucharest',90)],
               'Urziceni':[('Bucharest',85),('Vaslui',142),('Hirsova',98)],
               'Neamt':[('Iasi',87)],
               'Iasi':[('Neamt',87),('Vaslui',92)],
               'Vaslui':[('Iasi',92),('Urziceni',142)],
               'Hirsova':[('Urziceni',98),('Eforie',86)],
               'Eforie':[('Hirsova',86)]           
              }

class PathFinderAgent(object):

    #flag for stopping the route print statement on too low of a budget
    globalFlag = True

    def __init__(self, Map):
        self.map = Map
        self.route = []
        self.total_cost = 0

    #function for backtracking to a previous city
    def backtrack(self, start):
        if self.current_city is not start:
            # gets nodes of city before last
            nodes = self.map[self.route[-2]]
            # search for last city in nodes
            for c in nodes:
                if self.route[-1] in c:
                    # gets cost of last city
                    cost = c[1]
            # removes last city from route
            self.route = self.route[:-1]
            # subtracts last cities cost from total
            cost = self.total_cost - cost
            self.total_cost = cost
            # current_city is now the previous city before last
            next_city = self.route[-1]
            temp = next_city
            self.current_city = next_city
            return temp
        else:
            return start
    #function for creating a list to check against the visited list
    #it returns this is to be checked
    def checkVisit(self, cities):
        routeList = []
        for c in cities:
            routeTemp = list(self.route)
            if c not in routeTemp:
                routeTemp.append(c)
                routeList.append(routeTemp)
        return routeList

    #function to add visit to a city
    def addVisit(self, visited, route):
        return visited.append(copy.deepcopy(route))

    #function for temporarily adding a city to the route
    def tempRoute(self, city):
        route = []
        if city not in self.route:
            route = list(self.route)
            route.append(city)
            return route
        else:
            return -1 #returns -1 if city is already in the route

    def solve(self, start, end, budget):
        visited = [[start]]
        flag = True
        count = 0
        self.route.append(start)
        self.current_city = start
        while flag:
            count += 1
            if count == 1000:
                print "Please try again with a higher budget."
                global globalFlag
                globalFlag = False
                flag = False
            elif self.current_city == end:
                flag = False
            else:
                #Make a list of next cities: Observing
                cities = [city for city, cost in self.map[self.current_city]]
                #Make a list of next costs: Observing
                costs = [cost for city, cost in self.map[self.current_city]]
                #See if the destination is in the next cities' list
                try:
                    i = cities.index(end)
                    if (self.total_cost + costs[i]) <= budget:
                        self.current_city = end
                        self.route.append(self.current_city)
                        self.total_cost += costs[i]
                    else:
                        i = cities.index("Fail")
                        #backtracks twice because it needs to get two nodes away from the end city
                        self.backtrack(start)
                        self.backtrack(start)
                #if not then random walk    
                except ValueError:
                    temp = ""
                    next_city = "."
                    #loops until next city is found
                    while next_city != temp:
                        temp, temp2 = random.sample(self.map[self.current_city], 1)[0]
                        tRoute = self.tempRoute(temp)
                        if tRoute not in visited and tRoute is not -1:
                            next_city = temp
                            cost = temp2
                            # Checks if in the budget then adds the current cost to the total
                            if (self.total_cost + cost) <= budget:
                                self.total_cost += cost
                                # Set next city to the current city
                                self.current_city = next_city
                                # Add the current city to the route
                                self.route.append(self.current_city)
                                self.addVisit(visited, self.route)

                            else:
                                temp = self.backtrack(start)
                                next_city = temp
                        #checksif there are no more new cities to visit along the current path
                        elif not cities:
                            temp = self.backtrack(start)
                            next_city = temp
                        else:
                            routeList = self.checkVisit(cities)
                            visitFlag = True
                            #checks if route has already been tried
                            for i in routeList:
                                if not i in visited:
                                    visitFlag = False
                                    break
                            if visitFlag:
                                temp = self.backtrack(start)
                                next_city = temp



if __name__ == '__main__':
    global globalFlag
    globalFlag = True
    budget = int(raw_input("Please enter a budget for your trip:"))
    agent = PathFinderAgent(Romania_Map)
    agent.solve('Arad','Neamt', budget)
    if globalFlag:
        print agent.route, agent.total_cost